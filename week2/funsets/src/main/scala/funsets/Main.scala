package funsets

object Main extends App {
  import FunSets._
  val s1 = singletonSet(1)
  val s2 = singletonSet(2)
  val s3 = singletonSet(1000)

  val ss1 = union(s1, s2)
  val ss2 = union(ss1, s3)

  println(FunSets.toString(map(ss2, x => x+1)))

}
