package recfun

object Main {
  def main(args: Array[String]) {
    println("Pascal's Triangle")
    for (row <- 0 to 10) {
      for (col <- 0 to row)
        print(pascal(col, row) + " ")
      println()
    }
  }

  /**
   * Exercise 1
   */

  def pascal(c: Int, r: Int): Int = {
    if (c < 0 || r < 0) throw new IllegalArgumentException("negative value is not allowed")
    if (c > r) throw new IllegalArgumentException("column value more row value")
    if (c == 0 || c == r) 1
    else pascal(c - 1, r - 1) + pascal(c, r - 1)
  }

  /**
   * Exercise 2
   */
  def balance(chars: List[Char]): Boolean = {
    def processing(chars: List[Char], balancer: Integer): Boolean = {
      if (balancer < 0 || chars == Nil) balancer == 0
      else if (chars.head == '(') processing(chars.tail, balancer + 1)
      else if (chars.head == ')') processing(chars.tail, balancer - 1)
      else processing(chars.tail, balancer)
    }
    if (chars == Nil) true else processing(chars, 0)
  }

  /**
   * Exercise 3
   */
  def countChange(money: Int, coins: List[Int]): Int = {
    def processing(money: Int, coins: List[Int], amount: Int): Int = {
      if (coins == Nil) amount
      else if (coins.head > money) processing(money, coins.tail, amount)
      else if (money - coins.head == 0) processing(money, coins.tail, amount + 1)
      else processing(money, coins.tail, amount + countChange(money - coins.head, coins))
    }
    if(money == 0) 0 else processing(money, coins, 0)
  }

}
